import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LandingLayoutComponent} from './modules/landing/landing-layout/landing-layout.component';


const routes: Routes = [
  {path: '', component: LandingLayoutComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
