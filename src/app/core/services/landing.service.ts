import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class LandingService {

  constructor(private http: HttpClient) { }

  loadMovies() {
    return this.http.get('https://yts.mx/api/v2/list_movies.json');
  }
}
