import {Component, OnInit} from '@angular/core';
import {LandingService} from '../../../core/services/landing.service';
import {MovieModel} from '../../../shared/models/movie.model';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  movies: MovieModel = new MovieModel();

  constructor(private landingService: LandingService) {
  }

  loadMovies() {
    this.landingService.loadMovies().subscribe(res => this.movies = res.data.movies);
  }

  ngOnInit(): void {
    this.loadMovies();
  }

}
