import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LandingComponent } from './landing/landing.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { LandingLayoutComponent } from './landing-layout/landing-layout.component';
import {LandingService} from '../../core/services/landing.service';
import { MovieComponent } from './movie/movie.component';



@NgModule({
  declarations: [
    LandingComponent,
    SidebarComponent,
    LandingLayoutComponent,
    MovieComponent
  ],
  imports: [
    CommonModule,
  ],
  exports: [
    LandingComponent,
    SidebarComponent,
    MovieComponent
  ],
  providers: [
    LandingService
  ]
})
export class LandingModule { }
