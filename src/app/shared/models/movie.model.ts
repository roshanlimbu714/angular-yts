export class MovieModel {
  public background_image?: string;
  public background_image_original?: string;
  public date_uploaded?: string;
  public date_uploaded_unix?: string;
  public description_full?: string;
  public genres?: string;
  public id?: string;
  public imdb_code?: string;
  public language?: string;
  public large_cover_image?: string;
  public medium_cover_image?: string;
  public mpa_rating?: string;
  public rating?: string;
  public runtime?: string;
  public slug?: string;
  public small_cover_image?: string;
  public state?: string;
  public summary?: string;
  public synopsis?: string;
  public title?: string;
  public title_english?: string;
  public title_long?: string;
  public torrents?: string;
  public url?: string;
  public year?: string;
  public yt_trailer_code?: string;

  constructor(
    background_image?: string,
    background_image_original?: string,
    date_uploaded?: string,
    date_uploaded_unix?: string,
    description_full?: string,
    genres?: string,
    id?: string,
    imdb_code?: string,
    language?: string,
    large_cover_image?: string,
    medium_cover_image?: string,
    mpa_rating?: string,
    rating?: string,
    runtime?: string,
    slug?: string,
    small_cover_image?: string,
    state?: string,
    summary?: string,
    synopsis?: string,
    title?: string,
    title_english?: string,
    title_long?: string,
    torrents?: string,
    url?: string,
    year?: string,
    yt_trailer_code?: string,
  ) {
    this.background_image = background_image ? background_image : null;
    this.background_image_original = background_image_original ? background_image_original : null;
    this.date_uploaded = date_uploaded ? date_uploaded : null;
    this.date_uploaded_unix = date_uploaded_unix ? date_uploaded_unix : null;
    this.description_full = description_full ? description_full : null;
    this.genres = genres ? genres : null;
    this.id = id ? id : null;
    this.imdb_code = imdb_code ? imdb_code : null;
    this.language = language ? language : null;
    this.large_cover_image = large_cover_image ? large_cover_image : null;
    this.medium_cover_image = medium_cover_image ? medium_cover_image : null;
    this.mpa_rating = mpa_rating ? mpa_rating : null;
    this.rating = rating ? rating : null;
    this.runtime = runtime ? runtime : null;
    this.slug = slug ? slug : null;
    this.small_cover_image = small_cover_image ? small_cover_image : null;
    this.state = state ? state : null;
    this.summary = summary ? summary : null;
    this.synopsis = synopsis ? synopsis : null;
    this.title = title ? title : null;
    this.title_english = title_english ? title_english : null;
    this.title_long = title_long ? title_long : null;
    this.torrents = torrents ? torrents : null;
    this.url = url ? url : null;
    this.year = year ? year : null;
    this.yt_trailer_code = yt_trailer_code ? yt_trailer_code : null;
  }
}
